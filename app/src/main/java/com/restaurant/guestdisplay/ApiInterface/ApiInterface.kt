package com.restaurant.zing.ApiInterface

import com.google.gson.JsonObject
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface ApiInterface {


    @Headers("Content-type: application/json")
    @POST("future_online_order")
    fun futureOnlineOrders(@Body body: JsonObject): Call<AllDataResponse>



    companion object Factory {
        /* staging */
//        private const val BASE_URL = "http://3.14.236.148/api/pos/"
        private const val BASE_URL = "http://18.190.55.150/api/pos/"
        /* live */
//        private const val BASE_URL = ""

        private val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)

        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }

}