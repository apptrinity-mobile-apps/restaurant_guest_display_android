package com.restaurant.guestdisplay.ApiInterface

class FutureOnlineOrderDataResponse {

    /*"checkNumber": null,
    "deliveryStatus": 0,
    "dineInBehaviour": "delivery",
    "discountAmount": 0.4,
    "items_list": [
    {
        "itemId": "5fab7fde7de40f5c0fabf70c",
        "itemName": "a_poori"
    },
    {
        "itemId": "5fab7fde7de40f5c0fabf70d",
        "itemName": "a_onion dosa"
    },
    {
        "itemId": "5fab7fde7de40f5c0fabf70e",
        "itemName": "a_upma"
    }
    ],
    "orderDate": "11-11-2020",
    "orderNumber": 1,
    "orderTime": "06:08:30",
    "posOrderId": "5fab7fde7de40f5c0fabf70b",
    "scheduledDate": "11-10-2020",
    "scheduledEndTime": "04:30:00:AM",
    "scheduledStartTime": "02:30:00:AM",
    "status": 0,
    "subTotal": 25.0,
    "tableNumber": null,
    "taxAmount": 1.0,
    "tipAmount": 1.5,
    "totalAmount": 25.6*/

    val quantity: String? = null
    val price: String? = null
    val name: String? = null

    val itemName: String? = null
    val unitPrice: Double? = null
    val totalPrice: Double? = null


    val modifiersList: ArrayList<ModifiersDataResponse>? = null
    val specialRequestList: ArrayList<SpecialRequestDataResponse>? = null





}
