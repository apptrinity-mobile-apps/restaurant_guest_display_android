package com.restaurant.zing.ApiInterface

import com.restaurant.guestdisplay.ApiInterface.FutureOnlineOrderDataResponse

class AllDataResponse {
    val responseStatus: Int? = null
    val result: String? = null

    val futureOnlineOrderList: ArrayList<FutureOnlineOrderDataResponse>? = null
}
