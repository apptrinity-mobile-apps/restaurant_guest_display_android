package com.restaurant.guestdisplay.ApiInterface


import java.io.Serializable

class ModifiersDataResponse : Serializable {
    val modifierId: String? = null
    val modifierName: String? = null
    val modifierQty: Int? = null
    val modifierTotalPrice: Double? = null
    val modifierUnitPrice: Double? = null
    val modifierGroupId: String? = null
}
