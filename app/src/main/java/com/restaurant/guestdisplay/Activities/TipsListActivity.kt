package com.restaurant.guestdisplay.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.restaurant.guestdisplay.ApiInterface.TipsDataResponse
import com.restaurant.guestdisplay.Helpers.CustomTextView
import com.restaurant.guestdisplay.Helpers.CustomTextViewBold
import com.restaurant.guestdisplay.R

class TipsListActivity : AppCompatActivity() {

    private lateinit var loading_dialog: Dialog
    lateinit var rv_tipslist: RecyclerView
    lateinit var tv_tip_amount1: CustomTextView
    lateinit var tv_tip_amount2: CustomTextView
    lateinit var tv_tip_amount3: CustomTextView
    lateinit var tv_tip_amount4: CustomTextView
    lateinit var tv_tip_amount5: CustomTextView
    lateinit var ll_tip_amount1: CardView
    lateinit var ll_tip_amount2: CardView
    lateinit var ll_tip_amount3: CardView
    lateinit var ll_tip_amount4: CardView
    lateinit var ll_tip_amount5: CardView
    private var title_name = ""
    private var tipsarray = ""
    private var mSelectedunassignedIndex = -1
    var arrowdownup = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tips)

        initialize()

        try {
            if (intent.getStringExtra("title")!! != null || intent.getStringExtra("title")!! != "" || intent.getStringExtra(
                    "title"
                )!! != "null"
            ) {
                title_name = intent.getStringExtra("title")!!
            }
            Log.e("TITLENAME", title_name)
        } catch (e: Exception) {
            Log.e("EXCEPTION", e.toString())
        }

        try {
            if (intent.getStringExtra("tipsarray")!! != null || intent.getStringExtra("tipsarray")!! != "" || intent.getStringExtra(
                    "tipsarray"
                )!! != "null"
            ) {
                tipsarray = intent.getStringExtra("tipsarray")!!


                tv_tip_amount4.setText("$"+tipsarray)
                ll_tip_amount4.background =
                    ContextCompat.getDrawable(this!!, R.drawable.tip_amount_selected)
                tv_tip_amount4.setTextColor(
                    ContextCompat.getColor(
                        this!!, R.color.white
                    )
                )


                /*if(tipsarray.equals("1.0")){
                    tv_tip_amount1.setText("$"+tipsarray)
                    ll_tip_amount1.background =
                        ContextCompat.getDrawable(this!!, R.drawable.tip_amount_selected)
                    tv_tip_amount1.setTextColor(
                        ContextCompat.getColor(
                            this!!, R.color.white
                        )
                    )
                }else if(tipsarray.equals("2.0")){
                    tv_tip_amount2.setText("$"+tipsarray)
                    ll_tip_amount2.background =
                        ContextCompat.getDrawable(this!!, R.drawable.tip_amount_selected)
                    tv_tip_amount2.setTextColor(
                        ContextCompat.getColor(
                            this!!, R.color.white
                        )
                    )
                }else if(tipsarray.equals("3.0")){
                    tv_tip_amount3.setText("$"+tipsarray)
                    ll_tip_amount3.background =
                        ContextCompat.getDrawable(this!!, R.drawable.tip_amount_selected)
                    tv_tip_amount3.setTextColor(
                        ContextCompat.getColor(
                            this!!, R.color.white
                        )
                    )
                }else if(tipsarray.equals("4.0")){
                    tv_tip_amount4.setText("$"+tipsarray)
                    ll_tip_amount4.background =
                        ContextCompat.getDrawable(this!!, R.drawable.tip_amount_selected)
                    tv_tip_amount4.setTextColor(
                        ContextCompat.getColor(
                            this!!, R.color.white
                        )
                    )
                }else if(tipsarray.equals("5.0")){
                    tv_tip_amount5.setText("$"+tipsarray)
                    ll_tip_amount5.background =
                        ContextCompat.getDrawable(this!!, R.drawable.tip_amount_selected)
                    tv_tip_amount5.setTextColor(
                        ContextCompat.getColor(
                            this!!, R.color.white
                        )
                    )
                }else if(tipsarray > "5"){
                    ll_tip_amount1.visibility = View.GONE
                    ll_tip_amount2.visibility = View.VISIBLE
                    ll_tip_amount3.visibility = View.GONE
                    ll_tip_amount4.visibility = View.GONE
                    ll_tip_amount5.visibility = View.GONE
                    tv_tip_amount2.setText("$"+tipsarray)
                    ll_tip_amount2.background =
                        ContextCompat.getDrawable(this!!, R.drawable.tip_amount_selected)
                    tv_tip_amount2.setTextColor(
                        ContextCompat.getColor(
                            this!!, R.color.white
                        )
                    )
                }*/




            }

            Log.e("TIPSARRAY", "" + tipsarray)

           /* val respone: Array<TipsDataResponse>? =
                Gson().fromJson(tipsarray, Array<TipsDataResponse>::class.java)
            Log.e("TYPEARRAY2", "" + respone)
            for (s in respone!!) {
                System.out.println("File name: " + s.tipAmount)
            }*/


            //saveArray(respone!!, "itemdata", this)

            /*val adapter =
                FutureListAdapter(
                    this!!,
                    respone!!
                )
            rv_tipslist.adapter = adapter
            adapter.notifyDataSetChanged()
            rv_tipslist.visibility = View.VISIBLE
            // loadArray("itemdata", this)*/

        } catch (e: Exception) {
            Log.e("EXCEPTION", e.toString())
        }

        /*tv_custom_btn.setOnClickListener {
            if(arrowdownup == true){
                val intent = Intent(this, PaymentSuccessActivity::class.java)
                startActivity(intent)
            }

        }*/


    }

    private fun initialize() {
        tv_tip_amount1 = findViewById(R.id.tv_tip_amount1)
        tv_tip_amount2 = findViewById(R.id.tv_tip_amount2)
        tv_tip_amount3 = findViewById(R.id.tv_tip_amount3)
        tv_tip_amount4 = findViewById(R.id.tv_tip_amount4)
        tv_tip_amount5 = findViewById(R.id.tv_tip_amount5)
        ll_tip_amount1 = findViewById(R.id.ll_tip_amount1)
        ll_tip_amount2 = findViewById(R.id.ll_tip_amount2)
        ll_tip_amount3 = findViewById(R.id.ll_tip_amount3)
        ll_tip_amount4 = findViewById(R.id.ll_tip_amount4)
        ll_tip_amount5 = findViewById(R.id.ll_tip_amount5)



        /*rv_tipslist = findViewById(R.id.rv_tipslist)
        tv_custom_btn = findViewById(R.id.tv_custom_btn)
        val itemlistLinearLayoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        rv_tipslist.layoutManager = itemlistLinearLayoutManager
        rv_tipslist.hasFixedSize()*/


    }


   /* @SuppressLint("SetTextI18n")
    inner class FutureListAdapter(
        context: Context,
        list: Array<TipsDataResponse>
    ) :
        RecyclerView.Adapter<FutureListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: Array<TipsDataResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_tip_list, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {

            holder.tv_tip_amount.text = "$" + mList!![position].tipAmount.toString()

            holder.ll_tip_amount.setOnClickListener {



                Toast.makeText(
                    this@TipsListActivity!!,
                    mList!![position].tipAmount.toString(),
                    Toast.LENGTH_SHORT
                ).show()

                if (mSelectedunassignedIndex == -1 || mSelectedunassignedIndex != position) {
                    arrowdownup = true
                    Log.e("MSELECTEDIF", "" + mSelectedunassignedIndex)
                    mSelectedunassignedIndex = position
                    tv_custom_btn.isEnabled = true
                    tv_custom_btn.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                    tv_custom_btn.background = ContextCompat.getDrawable(
                        mContext!!,
                        R.drawable.tip_amount_selected
                    )

                } else if (mSelectedunassignedIndex == position) {

                    arrowdownup = false
                    Log.e("MSELECTEDELSE", "" + mSelectedunassignedIndex)
                    mSelectedunassignedIndex = -1
                    tv_custom_btn.isEnabled = false
                    tv_custom_btn.setTextColor(
                        ContextCompat.getColor(
                            mContext!!,
                            R.color.white
                        )
                    )
                }
                notifyDataSetChanged()
            }

            if (mSelectedunassignedIndex == position) {
                holder.ll_tip_amount.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.tip_amount_selected)
                holder.tv_tip_amount.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.white
                    )
                )

            } else {
                holder.ll_tip_amount.background =
                    ContextCompat.getDrawable(mContext!!, R.drawable.tip_amount_unselected)
                holder.tv_tip_amount.setTextColor(
                    ContextCompat.getColor(
                        mContext!!, R.color.item_tip_color
                    )
                )


            }


        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var ll_tip_amount = view.findViewById(R.id.ll_tip_amount) as LinearLayout
            var tv_tip_amount = view.findViewById(R.id.tv_tip_amount) as TextView


        }
    }


    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }


    fun saveArray(
        array: Array<TipsDataResponse>,
        arrayName: String,
        mContext: Context
    ): Boolean {
        val prefs = mContext.getSharedPreferences("preferencename", 0)
        val editor = prefs.edit()
        editor.putInt(arrayName + "_size", array.size)
        for (i in array.indices) editor.putString(arrayName + "_" + i, array[i].toString())
        return editor.commit()
    }


    fun loadArray(
        arrayName: String,
        mContext: Context
    ): Array<String?>? {
        val prefs = mContext.getSharedPreferences("preferencename", 0)
        val size = prefs.getInt(arrayName + "_size", 0)
        val array = arrayOfNulls<String>(size)
        for (i in 0 until size) array[i] = prefs.getString(arrayName + "_" + i, null)
        Log.e("ARRAYOFDATA", "" + array + "-------" + size)


        return array
    }*/


}