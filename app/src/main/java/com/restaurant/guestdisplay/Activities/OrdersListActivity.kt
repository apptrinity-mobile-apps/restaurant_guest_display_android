package com.restaurant.guestdisplay.Activities

import com.restaurant.guestdisplay.ApiInterface.FutureOnlineOrderDataResponse
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.restaurant.guestdisplay.ApiInterface.GetAllModifierListResponseList
import com.restaurant.guestdisplay.ApiInterface.ModifiersDataResponse
import com.restaurant.guestdisplay.ApiInterface.modifierIdList
import com.restaurant.guestdisplay.Helpers.CustomTextViewBold
import com.restaurant.guestdisplay.Helpers.CustomTextViewSemiBold
import com.restaurant.guestdisplay.Helpers.DataStorage
import com.restaurant.guestdisplay.R
import kotlinx.android.synthetic.main.layout_modifier_items.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class OrdersListActivity : AppCompatActivity() {

    private lateinit var loading_dialog:Dialog
    lateinit var rv_itemlist: RecyclerView
    lateinit var tv_order_subtotal: CustomTextViewBold
    lateinit var tv_order_tax: CustomTextViewBold
    lateinit var tv_order_total: CustomTextViewBold
    var device_token_stg = ""
    var device_type_stg = ""
    var dataStorage: DataStorage = DataStorage()
    private var title_name = ""
    private var itemarray = ""
    private var totalAmount = ""
    private var subTotal = ""
    private var discountAmount = ""
    private var taxAmount = ""
    var datalist = JsonArray()
    var datalistArray: ArrayList<FutureOnlineOrderDataResponse>? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initialize()
        device_token_stg = dataStorage.getFcmToken(this)
        device_type_stg = "Android"
        FirebaseInstanceId.getInstance().getInstanceId()
            .addOnSuccessListener(this, object : OnSuccessListener<InstanceIdResult?> {
                override fun onSuccess(instanceIdResult: InstanceIdResult?) {
                    val newToken: String = instanceIdResult!!.getToken()
                    Log.e("NewFcm Token", newToken)
                }
            })

        try {
            if (intent.getStringExtra("title")!! != null || intent.getStringExtra("title")!! != "" || intent.getStringExtra("title")!! != "null") {
                title_name = intent.getStringExtra("title")!!
            }
            Log.e("TITLENAME", title_name)
        }catch (e:Exception){
            Log.e("EXCEPTION", e.toString())
        }

        try {
            if (intent.getStringExtra("itemarray")!! != null || intent.getStringExtra("itemarray")!! != "" || intent.getStringExtra("itemarray")!! != "null") {
                itemarray = intent.getStringExtra("itemarray")!!
            }

            if (intent.getStringExtra("totalAmount")!! != null || intent.getStringExtra("totalAmount")!! != "" || intent.getStringExtra("totalAmount")!! != "null") {
                totalAmount = intent.getStringExtra("totalAmount")!!
            }
            if (intent.getStringExtra("subTotal")!! != null || intent.getStringExtra("subTotal")!! != "" || intent.getStringExtra("subTotal")!! != "null") {
                subTotal = intent.getStringExtra("subTotal")!!
            }
            if (intent.getStringExtra("discountAmount")!! != null || intent.getStringExtra("discountAmount")!! != "" || intent.getStringExtra("discountAmount")!! != "null") {
                discountAmount = intent.getStringExtra("discountAmount")!!
            }
            if (intent.getStringExtra("taxAmount")!! != null || intent.getStringExtra("taxAmount")!! != "" || intent.getStringExtra("taxAmount")!! != "null") {
                taxAmount = intent.getStringExtra("taxAmount")!!
            }

            tv_order_subtotal.setText(subTotal)
            tv_order_tax.setText(taxAmount)
            tv_order_total.setText(totalAmount)

            println("datalistACTIVITYBEFORE" +itemarray+"-"+totalAmount+"--"+subTotal+"---"+discountAmount+"----"+taxAmount)

            val respone: Array<FutureOnlineOrderDataResponse>? = Gson().fromJson(itemarray,Array<FutureOnlineOrderDataResponse>::class.java)
            Log.e("TYPEARRAY2", ""+respone)
            for (s in respone!!) {
                System.out.println("File name: " + s.name)
            }

            saveArray(respone!!,"itemdata",this)

           /* val allmodifierArray = ArrayList<GetAllModifierListResponseList>()
            for(i in 0 until respone.size){
                for (j in 0 until respone!![i].modifiersList!!.size) {
                    val modifierGroupIdListarray = ArrayList<modifierIdList>()
                    val modListIds = modifierIdList()
                    modListIds.id =
                        respone!![i].modifiersList!![j].modifierGroupId.toString()
                    modListIds.name = ""
                    modifierGroupIdListarray.add(modListIds)
                    val modifiersList = GetAllModifierListResponseList(
                        respone!![i].modifiersList!![j].modifierId.toString(),
                        respone!![i].modifiersList!![j].modifierName.toString(),
                        respone!![i].modifiersList!![j].modifierTotalPrice!!,
                        modifierGroupIdListarray
                    )
                    allmodifierArray.add(modifiersList)
                }

                for (k in 0 until respone!![i].specialRequestList!!.size) {
                    val modifierGroupIdListarray = ArrayList<modifierIdList>()
                    val modListIds = modifierIdList()
                    modListIds.id = ""
                    modListIds.name = ""
                    modifierGroupIdListarray.add(modListIds)
                    val specialRequestList = GetAllModifierListResponseList(
                        "",
                        respone!![i].specialRequestList!![k].name.toString(),
                        respone!![i].specialRequestList!![k].requestPrice!!,
                        modifierGroupIdListarray
                    )
                    allmodifierArray.add(specialRequestList)
                }


            }*/




            val adapter =
                FutureListAdapter(
                    this!!,
                    respone!!
                )
            rv_itemlist.adapter = adapter
            adapter.notifyDataSetChanged()
            rv_itemlist.visibility = View.VISIBLE


            loadArray("itemdata",this)

        }catch (e:Exception){
            Log.e("EXCEPTION", e.toString())
        }


    }

    private fun initialize() {
        loadingDialog()
        //iv_back = findViewById(R.id.iv_back)



        rv_itemlist = findViewById(R.id.rv_item_list)
        tv_order_subtotal = findViewById(R.id.tv_order_subtotal)
        tv_order_tax = findViewById(R.id.tv_order_tax)
        tv_order_total = findViewById(R.id.tv_order_total)



        val itemlistLinearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_itemlist.layoutManager = itemlistLinearLayoutManager
        rv_itemlist.hasFixedSize()


    }



    @SuppressLint("SetTextI18n")
    inner class FutureListAdapter(
        context: Context,
        list: Array<FutureOnlineOrderDataResponse>
    ) :
        RecyclerView.Adapter<FutureListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: Array<FutureOnlineOrderDataResponse>? = null
        internal var ll_modifiersdata: LinearLayout? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_menu_list, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {

            val item_untiprice = "%.2f".format(mList!![position].unitPrice)
            val item_totalprice = "%.2f".format(mList!![position].totalPrice)


            holder.tv_item_name.text = mList!![position].itemName.toString()
            holder.tv_item_qty.text = mList!![position].quantity.toString()
            holder.tv_item_price.text = item_untiprice
            holder.tv_item_total.text = item_totalprice


            val allmodifierArray = ArrayList<GetAllModifierListResponseList>()
            for (j in 0 until mList!![position].modifiersList!!.size) {
                val modifierGroupIdListarray = ArrayList<modifierIdList>()
                val modListIds = modifierIdList()
                modListIds.id =
                    mList!![position].modifiersList!![j].modifierGroupId.toString()
                modListIds.name = ""
                modifierGroupIdListarray.add(modListIds)
                val modifiersList = GetAllModifierListResponseList(
                    mList!![position].modifiersList!![j].modifierId.toString(),
                    mList!![position].modifiersList!![j].modifierName.toString(),
                    mList!![position].modifiersList!![j].modifierTotalPrice!!,
                    modifierGroupIdListarray
                )
                allmodifierArray.add(modifiersList)
            }

            for (k in 0 until mList!![position].specialRequestList!!.size) {
                val modifierGroupIdListarray = ArrayList<modifierIdList>()
                val modListIds = modifierIdList()
                modListIds.id = ""
                modListIds.name = ""
                modifierGroupIdListarray.add(modListIds)
                val specialRequestList = GetAllModifierListResponseList(
                    "",
                    mList!![position].specialRequestList!![k].name.toString(),
                    mList!![position].specialRequestList!![k].requestPrice!!,
                    modifierGroupIdListarray
                )
                allmodifierArray.add(specialRequestList)
            }
            Log.e("MODIFIERSLENGTHADAPTER",""+allmodifierArray.size)


            if (allmodifierArray!!.size > 0) {
                holder.layout_modifiers_value!!.removeAllViews()
                for (i in 0 until allmodifierArray.size) {
                    ll_modifiersdata = LinearLayout(mContext)
                    ll_modifiersdata!!.orientation = LinearLayout.HORIZONTAL
                    val layoutView = LayoutInflater.from(mContext).inflate(R.layout.layout_modifier_items, holder.layout_modifiers_value, false)

                    val tv_modifier_item_name: TextView = layoutView.findViewById(R.id.tv_modifier_item_name)
                    val tv_modifier_item_quantity: TextView = layoutView.findViewById(R.id.tv_modifier_item_qty)
                    val tv_modifier_item_price: TextView = layoutView.findViewById(R.id.tv_modifier_item_price)
                    val tv_modifier_item_total: TextView = layoutView.findViewById(R.id.tv_modifier_item_total)



                    tv_modifier_item_name.setText(allmodifierArray.get(i).name)
                    tv_modifier_item_total.setText("%.2f".format(allmodifierArray.get(i).price))

                    ll_modifiersdata!!.addView(layoutView)
                    holder.layout_modifiers_value!!.addView(ll_modifiersdata)
                }
            } else{
                holder.layout_modifiers_value!!.removeAllViews()

            }






        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_item_name = view.findViewById(R.id.tv_item_name) as TextView
            var tv_item_qty = view.findViewById(R.id.tv_item_qty) as TextView
            var tv_item_price = view.findViewById(R.id.tv_item_price) as TextView
            var tv_item_total = view.findViewById(R.id.tv_item_total) as TextView
            var layout_modifiers_value = view.findViewById(R.id.layout_modifiers_value) as LinearLayout



        }
    }


    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }


    fun saveArray(
        array: Array<FutureOnlineOrderDataResponse>,
        arrayName: String,
        mContext: Context
    ): Boolean {
        val prefs = mContext.getSharedPreferences("preferencename", 0)
        val editor = prefs.edit()
        editor.putInt(arrayName + "_size", array.size)
        for (i in array.indices) editor.putString(arrayName + "_" + i, array[i].toString())
        return editor.commit()
    }


    fun loadArray(
        arrayName: String,
        mContext: Context
    ): Array<String?>? {
        val prefs = mContext.getSharedPreferences("preferencename", 0)
        val size = prefs.getInt(arrayName + "_size", 0)
        val array = arrayOfNulls<String>(size)
        for (i in 0 until size) array[i] = prefs.getString(arrayName + "_" + i, null)
        Log.e("ARRAYOFDATA",""+array+"-------"+size)


        return array
    }


}