package com.restaurant.guestdisplay.Helpers

import android.app.ActivityOptions
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.JsonArray
import com.google.gson.JsonParser
import com.restaurant.guestdisplay.Activities.OrdersListActivity
import com.restaurant.guestdisplay.Activities.PaymentSuccessActivity
import com.restaurant.guestdisplay.Activities.TipsListActivity
import com.restaurant.guestdisplay.R
import org.json.JSONObject


class MyFirebaseMessagingService : FirebaseMessagingService() {
    var dataStorage: DataStorage = DataStorage()

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.e("NEW_TOKEN", s)
        dataStorage.saveFcmToken(applicationContext, s)

    }


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("TAG", "From: ${remoteMessage.from}")

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d("TAG", "Message data payload: ${remoteMessage.data}")

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                //scheduleJob()
                sendNotification(remoteMessage,"","")
            } else {
                // Handle message within 10 seconds
                //handleNow()
                val map = remoteMessage.data
                handleDataMessage(remoteMessage.toString())
            }
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d("TAG", "Message Notification Body: ${it.body}")
            Log.d("TAG", "Message Notification Body: ${it.title}")

            sendNotification(remoteMessage,it.title!!,it.body!!)
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }







    /*override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        if (remoteMessage.data.isNotEmpty()) {
            try {
                val params = remoteMessage.data
                Log.e("JSON_OBJECT", params.toString())
                val object1 = JSONObject(params as Map<*, *>)
                Log.e("JSON_OBJECT", object1.toString())
                //  bitmap = getBitmapFromUrl(object1.getString("image"))
                //val type = object1.getString("type")
                sendNotification(remoteMessage *//*bitmap!!,message*//*)

                try {
                    val map = remoteMessage.data
                    handleDataMessage(map)
                } catch (e: Exception) {
                    Log.e("TAG", "Exception: " + e.message)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }*/


    fun sendNotification(remoteMessage:RemoteMessage?,title:String,message:String) {
        Log.e("remoteMessage", remoteMessage.toString())

        Log.e("remoteMessage_rrrr", remoteMessage.toString())
        var typeorder = ""
        var typetips = ""
        var totalAmount = ""
        var subTotal = ""
        var discountAmount = ""
        var taxAmount = ""
        var dataType = ""
        var datalist = JsonArray()
        var object1 = JSONObject()
        var id = ""
        var name = ""
        if (remoteMessage!!.data.isNotEmpty()) {
            try {
                val params = remoteMessage.data
                Log.e("NOTIFICATIONRAWDATA", params.toString())
                 object1 = JSONObject(params as Map<*, *>)
                Log.e("JSON_OBJECT", object1.toString())

                if(object1.getString("dataType") != null || object1.getString("dataType") !="" || object1.getString("dataType") != "null"){
                    dataType = object1.getString("dataType")
                }

               // dataType = object1.getString("dataType")

                Log.e("TYPETIPS", typetips)

              /*  val jsonP = JsonParser()
                datalist = jsonP.parse(typeorder) as JsonArray
                for (i in 0 until datalist.size()){
                    println("datalist" +datalist[i])
                }*/


            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationId = 1
        val channelId = "channel-01"
        val channelName = "Guestdisplay"
        val importance = NotificationManager.IMPORTANCE_HIGH

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(
                channelId, channelName, importance)
            notificationManager.createNotificationChannel(mChannel)
        }

        val sound: Uri =
            Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + applicationContext.getPackageName() + "/" + RingtoneManager.TYPE_NOTIFICATION)
        val mBuilder = NotificationCompat.Builder(this, channelId)
            .setContentTitle(title)
            .setContentText(message)
            .setSound(sound)
            .setAutoCancel(true)
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setSmallIcon(R.drawable.ic_email);
            //mBuilder.setColor(getResources().getColor(R.color.white));
        } else {
            mBuilder.setSmallIcon(R.drawable.ic_email);
        }


        if(dataType.equals("tip")){

            if(object1.getString("tipAmount") != null || object1.getString("tipAmount") !="" || object1.getString("tipAmount") != "null"){
                typetips = object1.getString("tipAmount")
            }

            Log.e("TYPETIPSFIREBASE",typetips)
            val resultIntent = Intent(this, TipsListActivity::class.java)
            resultIntent.putExtra("title", title)
            resultIntent.putExtra("tipsarray", typetips)
            // resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addNextIntent(resultIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            mBuilder.setContentIntent(resultPendingIntent)
            mBuilder.notification.contentIntent.send()
            // resultIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val options =
                ActivityOptions.makeCustomAnimation(this, R.anim.fade_in, R.anim.fade_out)
            this.startActivity(resultIntent, options.toBundle())
        }else if(dataType.equals("order")){

            if(object1.getString("itemsList") != null || object1.getString("itemsList") !="" || object1.getString("itemsList") != "null"){
                typeorder = object1.getString("itemsList")
            }
            if(object1.getString("totalAmount") != null || object1.getString("totalAmount") !="" || object1.getString("totalAmount") != "null"){
                totalAmount = object1.getString("totalAmount")
            }
            if(object1.getString("subTotal") != null || object1.getString("subTotal") !="" || object1.getString("subTotal") != "null"){
                subTotal = object1.getString("subTotal")
            }
            if(object1.getString("taxAmount") != null || object1.getString("taxAmount") !="" || object1.getString("taxAmount") != "null"){
                taxAmount = object1.getString("taxAmount")
            }
            if(object1.getString("discountAmount") != null || object1.getString("discountAmount") !="" || object1.getString("discountAmount") != "null"){
                discountAmount = object1.getString("discountAmount")
            }


            val resultIntent = Intent(this, OrdersListActivity::class.java)
            resultIntent.putExtra("title", title)
            resultIntent.putExtra("itemarray", typeorder)
            resultIntent.putExtra("totalAmount", totalAmount)
            resultIntent.putExtra("subTotal", subTotal)
            resultIntent.putExtra("discountAmount", discountAmount)
            resultIntent.putExtra("taxAmount", taxAmount)
            // resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addNextIntent(resultIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            mBuilder.setContentIntent(resultPendingIntent)
            mBuilder.notification.contentIntent.send()
            // resultIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val options =
                ActivityOptions.makeCustomAnimation(this, R.anim.fade_in, R.anim.fade_out)
            this.startActivity(resultIntent, options.toBundle())
        } else if(dataType.equals("payment")){
            val resultIntent = Intent(this, PaymentSuccessActivity::class.java)
            // resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addNextIntent(resultIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            mBuilder.setContentIntent(resultPendingIntent)
            mBuilder.notification.contentIntent.send()
            // resultIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val options =
                ActivityOptions.makeCustomAnimation(this, R.anim.fade_in, R.anim.fade_out)
            this.startActivity(resultIntent, options.toBundle())
        }

        //mBuilder.setContentIntent(resultPendingIntent)
      // notificationManager.notify(notificationId, mBuilder.build())

    }

    private fun handleDataMessage(map: String) {
        Log.e("TAG", "push json: " + map.toString())

        try {

            //val type = map["type"]
           // val alert = map["alert"]

        } catch (e: Exception) {
            Log.e("TAG", "Exception: " + e.message)
        }


    }
}